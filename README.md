# WinderNet PHP Framework - Module `Common`

The `Common` module of the `WinderNet PHP Framework`. It provides all the basic functionalities of the framework, which are used by most other modules.

### Supported PHP Versions
The module is based on PHP 8.0. Updates to other PHP versions will be done in future.

### Installation
Please, visit the corresponding [wiki page](https://gitlab.com/windernet/php/framework/-/wikis/Installation) for a proper guide on how to install the module.

### Usage
Please, visit the corresponding [wiki page](https://gitlab.com/windernet/php/framework/-/wikis/How-to-Use) for a proper guide on how to use the module.

### Support
For bugs and requests, please use the [GitLab issue tracker](https://gitlab.com/windernet/php/framework-module-common/-/issues). For questions, please use the [GitLab wiki](https://gitlab.com/windernet/php/framework/-/wikis/home) or see in file documentation.

### License
This project is licensed under the **MIT Licence**. [Learn more](https://opensource.org/license/mit/)
