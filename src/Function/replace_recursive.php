<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Common\Function;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use function array_key_exists;
use function is_array;
use function is_object;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     FUNCTION                                                                     \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * Recursively replaces all previous values by all following values.
 *
 * @note Scalar values will replace all previous values.
 * @note Array and object values will replace all previous scalar values.
 * @note If either the previous value or the current value is an array and the other is an object, the resulting value will be an object.
 * @note If the previous value and the current value are both arrays or objects (and of the same type), the elements (or properties) of previous value
 *       will be recursively replaced by the elements (or properties) of current value in the following way:
 *       If an index (or key) exists only in one value, its element (or property) will be assigned to the resulting value.
 *       Else, if an index (or key) exists in both values, the elements (or properties) will be recursively replaced.
 *
 * @param mixed ...$values - All the values, which shall be recursively replaced.
 *
 * @return mixed - {@see null}, if no {@param $values} have been provided.
 *                 Otherwise, the resulting value, after all {@param $values} have been recursively replaced.
 *
 * @since 0.1.0
 */
function replace_recursive(mixed ...$values): mixed
{
    $isObject       = false;
    $resultingValue = null;

    foreach ($values as $value) {
        // Scalar values replace everything and will be replaced by everything.
        if ((!is_array($value) && !is_object($value)) || (!is_array($resultingValue) && !is_object($resultingValue))) {
            $resultingValue = $value;

            continue;
        }

        // Do some preparations before replacing.
        if (is_object($value)) {
            $isObject       = true;
            $resultingValue = is_object($resultingValue) ? (array)$resultingValue : array (); // If it's not an object, it will be replaced.
            $value          = (array)$value;
        } else {
            $resultingValue = is_array($resultingValue)  ? $resultingValue        : array (); // If it's not an array, it will be replaced.
        }

        // Replace values as arrays.
        foreach ($value as $index => $element) {
            $resultingValue[$index] = array_key_exists($index, $resultingValue) ? replace_recursive($resultingValue[$index], $element) : $element;
        }

        // Cast resulting value back into an object, if current value was one.
        if ($isObject) {
            $isObject       = false;
            $resultingValue = (object)$resultingValue;
        }
    }

    return $resultingValue;
}
