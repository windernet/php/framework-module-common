<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace WinderNet\Common;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use WinderNet\Common\Exception\ForbiddenException;
use WinderNet\Common\Exception\InitializationException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      CLASS                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * This class provides functionalities for dependency management.
 *
 * @since 0.2.0
 */
class DependencyManager
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                        DEPENDENCY MANAGEMENT CONSTANTS                                                       \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * The keyword "allowOverride".
     *
     * @since 0.2.0
     */
    protected const KEYWORD_ALLOW_OVERRIDE = 'allowOverride';

    /**
     * The keyword "allowUnset".
     *
     * @since 0.2.0
     */
    protected const KEYWORD_ALLOW_UNSET    = 'allowUnset';

    /**
     * The keyword "dependency".
     *
     * @since 0.2.0
     */
    protected const KEYWORD_DEPENDENCY     = 'dependency';

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                       DEPENDENCY MANAGEMENT PROPERTIES                                                       \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * All dependencies of the application.
     *
     * @since 0.2.0
     */
    protected static ?object $dependencies = null;

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                        DEPENDENCY MANAGEMENT FUNCTIONS                                                       \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Gets a dependency.
     *
     * @param string $name    - The name of the dependency, which shall be returned.
     * @param mixed  $default - An optional default value, in case the dependency is not known.
     *
     * @return mixed - The dependency, if it is known.
     *                 {@param $default}, otherwise.
     *
     * @since 0.2.0
     *
     * @example $myDependency = DependencyManager::get('my-dependency');
     */
    public static function get(string $name, mixed $default = null): mixed
    {
        return static::$dependencies->{$name}->{static::KEYWORD_DEPENDENCY} ?? $default;
    }

    /**
     * Sets a dependency.
     *
     * @param string  $name          - The name of the dependency, which shall be set.
     * @param mixed   $dependency    - The dependency, which shall be set.
     * @param boolean $allowOverride - Whether it is allowed to override the dependency.
     * @param boolean $allowUnset    - Whether it is allowed to delete the dependency.
     *
     * @throws ForbiddenException - If the dependency already exists and overriding it is not allowed.
     *
     * @since 0.2.0
     *
     * @example DependencyManager::set('my-dependency', $myDependency);
     */
    public static function set(string $name, mixed $dependency, bool $allowOverride = false, bool $allowUnset = false): void
    {
        // Initialize the dependency manager.
        static::initialize();

        // Check, whether the dependency already exists and overriding it is disallowed.
        if (false === (static::$dependencies->{$name}->{static::KEYWORD_ALLOW_OVERRIDE} ?? null)) {
            throw new ForbiddenException('Overriding the dependency "' . $name . '" is forbidden.');
        }

        // Set the dependency.
        static::$dependencies->{$name} = (object)array (
            static::KEYWORD_ALLOW_OVERRIDE => $allowOverride,
            static::KEYWORD_ALLOW_UNSET    => $allowUnset,
            static::KEYWORD_DEPENDENCY     => $dependency
        );
    }

    /**
     * Unsets a dependency.
     *
     * @param string $name - The name of the dependency, which shall be unset.
     *
     * @throws ForbiddenException - If unsetting the dependency is not allowed.
     *
     * @since 0.2.0
     *
     * @example DependencyManager::unset('my-dependency');
     */
    public static function unset(string $name): void
    {
        // Check, whether unsetting the dependency is disallowed.
        if (false === (static::$dependencies->{$name}->{static::KEYWORD_ALLOW_UNSET} ?? null)) {
            throw new ForbiddenException('Unsetting the dependency "' . $name . '" is forbidden.');
        }

        // Unset the dependency.
        unset (static::$dependencies->{$name});
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                           INITIALIZATION FUNCTIONS                                                           \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Initializes this dependency manager.
     *
     * @throws InitializationException - If initialization failed, due to any reason.
     *
     * @since 0.2.0
     */
    protected static function initialize(): void
    {
        // Do nothing, if the dependency manager has already been initialized.
        if (null !== static::$dependencies) {
            return;
        }

        // Initialize the dependency manager.
        static::$dependencies = (object)array ();
    }
}
