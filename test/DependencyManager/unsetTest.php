<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use PHPUnit\Framework\TestCase;

use Test\WinderNet\Common\ResetTrait;

use WinderNet\Common\DependencyManager;
use WinderNet\Common\Exception\ForbiddenException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see DependencyManager::unset()
 */
final class unsetTest extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ResetTrait; // Implies {@see ReflectionTrait}

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustUnsetADependencyWhenUninitialized(): void
    {
        // Run the test.
        DependencyManager::unset('MyDependency');

        // Assert the test.
        $this->assertNull(self::$class->getStaticPropertyValue('dependencies'));
    }

    /**
     */
    public function testMustUnsetAnUnknownDependency(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array ());

        // Run the test.
        DependencyManager::unset('MyDependency');

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectNotHasProperty('MyDependency', $dependencies);
    }

    /**
     */
    public function testMustUnsetAKnownDependencyIfUnsettingItIsAllowed(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array (
            'MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => true, 'dependency' => true)
        ));

        // Run the test.
        DependencyManager::unset('MyDependency');

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectNotHasProperty('MyDependency', $dependencies);
    }

    /**
     */
    public function testMustNotUnsetAKnownDependencyIfUnsettingItIsNotAllowed(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array (
            'MyDependency' => (object)array ('allowOverride' => true, 'allowUnset' => false, 'dependency' => false)
        ));

        // Run the test.
        try {
            DependencyManager::unset('MyDependency');
        } catch (ForbiddenException) {}

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
    }

    /**
     */
    public function testMustThrowAnExceptionIfUnsettingADependencyIsNotAllowed(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array (
            'MyDependency' => (object)array ('allowOverride' => true, 'allowUnset' => false, 'dependency' => false)
        ));

        // Assert the test.
        $this->expectException(ForbiddenException::class);

        // Run the test.
        DependencyManager::unset('MyDependency');
    }
}
