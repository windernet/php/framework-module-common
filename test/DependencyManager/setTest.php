<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use PHPUnit\Framework\TestCase;

use Test\WinderNet\Common\ResetTrait;

use WinderNet\Common\DependencyManager;
use WinderNet\Common\Exception\ForbiddenException;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see DependencyManager::set()
 */
final class setTest extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ResetTrait; // Implies {@see ReflectionTrait}

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustInitializeDependenciesIfTheyAreNotInitialized(): void
    {
        // Assert preparations.
        $this->assertNull(self::$class->getStaticPropertyValue('dependencies'));

        // Run the test.
        DependencyManager::set('MyDependency', true);

        // Assert the test.
        $this->assertNotNull(self::$class->getStaticPropertyValue('dependencies'));
    }

    /**
     */
    public function testMustNotInitializeDependenciesIfTheyAreAlreadyInitialized(): void
    {
        // Prepare the test.
        $dependencies = (object)array (
            'MyDependency1' => (object)array('allowOverride' => false, 'allowUnset' => false, 'dependency' => 1),
            'MyDependency2' => (object)array('allowOverride' => false, 'allowUnset' => false, 'dependency' => 2)
        );

        self::$class->setStaticPropertyValue('dependencies', (object)array ('MyDependency1' => $dependencies->MyDependency1));

        // Run the test.
        DependencyManager::set('MyDependency2', 2);

        // Assert the test.
        $this->assertEquals($dependencies, self::$class->getStaticPropertyValue('dependencies'));
    }

    /**
     */
    public function testMustSetDependency(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('dependency', $dependencies->MyDependency);
        $this->assertTrue($dependencies->MyDependency->dependency);
    }

    /**
     */
    public function testMustSetWhetherDeletionOfDependencyIsAllowedToFalseIfNoValueIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowUnset', $dependencies->MyDependency);
        $this->assertFalse($dependencies->MyDependency->allowUnset);
    }

    /**
     */
    public function testMustSetWhetherDeletionOfDependencyIsAllowedToFalseIfFalseIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true, allowUnset: false);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowUnset', $dependencies->MyDependency);
        $this->assertFalse($dependencies->MyDependency->allowUnset);
    }

    /**
     */
    public function testMustSetWhetherDeletionOfDependencyIsAllowedToTrueIfTrueIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true, allowUnset: true);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowUnset', $dependencies->MyDependency);
        $this->assertTrue($dependencies->MyDependency->allowUnset);
    }

    /**
     */
    public function testMustSetWhetherOverrideOfDependencyIsAllowedToFalseIfNoValueIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowOverride', $dependencies->MyDependency);
        $this->assertFalse($dependencies->MyDependency->allowOverride);
    }

    /**
     */
    public function testMustSetWhetherOverrideOfDependencyIsAllowedToFalseIfFalseIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true, allowOverride: false);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowOverride', $dependencies->MyDependency);
        $this->assertFalse($dependencies->MyDependency->allowOverride);
    }

    /**
     */
    public function testMustSetWhetherOverrideOfDependencyIsAllowedToTrueIfTrueIsProvided(): void
    {
        // Run the test.
        DependencyManager::set('MyDependency', true, allowOverride: true);

        // Assert the test.
        $dependencies = self::$class->getStaticPropertyValue('dependencies');

        $this->assertIsObject($dependencies);
        $this->assertObjectHasProperty('MyDependency', $dependencies);
        $this->assertIsObject($dependencies->MyDependency);
        $this->assertObjectHasProperty('allowOverride', $dependencies->MyDependency);
        $this->assertTrue($dependencies->MyDependency->allowOverride);
    }

    /**
     */
    public function testMustAllowOverwritingADependencyIfOverwritingItIsAllowed(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array (
            'MyDependency' => (object)array ('allowOverride' => true, 'allowUnset' => false, 'dependency' => true)
        ));

        // Run the test.
        DependencyManager::set('MyDependency', false);

        // Assert the test.
        $this->assertEquals(
            (object)array ('MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => false, 'dependency' => false)),
            self::$class->getStaticPropertyValue('dependencies')
        );
    }

    /**
     */
    public function testMustNotAllowOverwritingADependencyIfOverwritingItIsNotAllowed(): void
    {
        // Prepare the test.
        $dependencies = (object)array ('MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => true, 'dependency' => false));

        self::$class->setStaticPropertyValue('dependencies', $dependencies);

        // Run the test.
        try {
            DependencyManager::set('MyDependency', true);
        } catch (ForbiddenException) {}

        // Assert the test.
        $this->assertEquals($dependencies, self::$class->getStaticPropertyValue('dependencies'));
    }

    /**
     */
    public function testMustThrowAnExceptionIfOverwritingADependencyIsNotAllowed(): void
    {
        // Prepare the test.
        $dependencies = (object)array ('MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => true, 'dependency' => false));

        self::$class->setStaticPropertyValue('dependencies', $dependencies);

        // Assert the test.
        $this->expectException(ForbiddenException::class);

        // Run the test.
        DependencyManager::set('MyDependency', true);
    }
}
