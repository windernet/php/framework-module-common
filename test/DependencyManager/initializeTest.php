<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use ReflectionException;

use PHPUnit\Framework\TestCase;

use Test\WinderNet\Common\ResetTrait;

use WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see DependencyManager::initialize()
 */
final class initializeTest extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ResetTrait { // Implies {@see ReflectionTrait}
        setUpBeforeClass as _setUpBeforeClass;
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                              PHP UNIT FUNCTIONS                                                              \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * Prepares all upcoming tests of this test class, before their execution.
     *
     * @throws ReflectionException - If {@see self::$methodName} is invalid.
     */
    public static function setUpBeforeClass(): void
    {
        // Set up traits.
        self::$methodName = "initialize";

        self::_setUpBeforeClass();
    }

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     * @throws ReflectionException - If {@see self::$method} cannot be invoked.
     */
    public function testMustInitializeDependenciesIfTheyAreNotInitialized(): void
    {
        // Assert preparations.
        $this->assertNull(self::$class->getStaticPropertyValue('dependencies'));

        // Run the test.
        self::$method->invoke(null);

        // Assert the test.
        $this->assertIsObject(self::$class->getStaticPropertyValue('dependencies'));
    }

    /**
     * @throws ReflectionException - If {@see self::$method} cannot be invoked.
     */
    public function testMustNotInitializeDependenciesIfTheyAreAlreadyInitialized(): void
    {
        // Prepare the test.
        $dependencies = (object)array ('MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => false, 'dependency' => true));

        self::$class->setStaticPropertyValue('dependencies', $dependencies);

        // Run the test.
        self::$method->invoke(null);

        // Assert the test.
        $this->assertEquals($dependencies, self::$class->getStaticPropertyValue('dependencies'));
    }
}
