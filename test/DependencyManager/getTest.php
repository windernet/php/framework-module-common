<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use PHPUnit\Framework\TestCase;

use Test\WinderNet\Common\ResetTrait;

use WinderNet\Common\DependencyManager;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see DependencyManager::get()
 */
final class getTest extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                 USED TRAITS                                                                  \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    use ResetTrait; // Implies {@see ReflectionTrait}

    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustReturnNullIfNotInitializedAndNoDefaultParameterIsProvided(): void
    {
        // Run and assert the test.
        $this->assertNull(DependencyManager::get('MyDependency'));
    }

    /**
     */
    public function testMustReturnDefaultParameterIfNotInitializedAndADefaultParameterIsProvided(): void
    {
        // Run and assert the test.
        $this->assertTrue(DependencyManager::get('MyDependency', true));
    }

    /**
     */
    public function testMustReturnNullIfDependencyIsNotKnownAndNoDefaultParameterIsProvided(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array ());

        // Run and assert the test.
        $this->assertNull(DependencyManager::get('MyDependency'));
    }

    /**
     */
    public function testMustReturnDefaultParameterIfDependencyIsNotKnownAndADefaultParameterIsProvided(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array ());

        // Run and assert the test.
        $this->assertTrue(DependencyManager::get('MyDependency', true));
    }

    /**
     */
    public function testMustReturnDependencyIfDependencyIsKnown(): void
    {
        // Prepare the test.
        self::$class->setStaticPropertyValue('dependencies', (object)array (
            'MyDependency' => (object)array ('allowOverride' => false, 'allowUnset' => false, 'dependency' => true)
        ));

        // Run and assert the test.
        $this->assertTrue(DependencyManager::get('MyDependency'));
    }
}
