<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework-module-common/-/blob/main/LICENSE
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                     NAMESPACE                                                                    \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

namespace Test\WinderNet\Common\Function;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                      USAGES                                                                      \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

use PHPUnit\Framework\TestCase;

use function WinderNet\Common\Function\replace_recursive;

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                       TEST                                                                       \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

/**
 * @see replace_recursive()
 */
final class replace_recursiveTest extends TestCase
{
    //**********************************************************************************************************************************************\\
    //                                                                                                                                              \\
    //                                                                TEST FUNCTIONS                                                                \\
    //                                                                                                                                              \\
    //**********************************************************************************************************************************************\\

    /**
     */
    public function testMustReplaceAnyValueByScalarValue(): void
    {
        // Prepare the test.
        $scalarValues = array (null, false, true, -65, 0, 5, -6.6, 3.5, '', 'a string');
        $values       = array (...$scalarValues, array (), array (...$scalarValues), (object)array (), (object)array ('property' => 'value'));

        foreach ($scalarValues as $scalarValue) {
            foreach ($values as $value) {
                // Run and assert the test.
                $this->assertEquals($scalarValue, replace_recursive($value, $scalarValue));
            }
        }
    }

    /**
     */
    public function testMustReplaceScalarValueByAnyValue(): void
    {
        // Prepare the test.
        $scalarValues = array (null, false, true, -65, 0, 5, -6.6, 3.5, '', 'a string');
        $values       = array (...$scalarValues, array (), array (...$scalarValues), (object)array (), (object)array ('property' => 'value'));

        foreach ($scalarValues as $scalarValue) {
            foreach ($values as $value) {
                // Run and assert the test.
                $this->assertEquals($value, replace_recursive($scalarValue, $value));
            }
        }
    }

    /**
     */
    public function testMustReplaceArraysByObjectsAndViceVersa(): void
    {
        // Prepare the test.
        $arrays  = array (array (), array (1, 2, 3), array (3 => 4, 4 => 5, 5 => 6), array ('This', 'is', 'an', 'array', '.'));
        $objects = array (
            (object)array (),
            (object)array ('property 1' => 'value 1', 'property 2' => 'value 2', 'prop1' => 'val1', 'prop2' => 'val2'),
            (object)array ('property 1' => 'value'),
            (object)array ('prop2' => 'val')
        );

        foreach ($arrays as $array) {
            foreach ($objects as $object) {
                // Run and assert the test.
                $this->assertEquals($object, replace_recursive($array , $object));
                $this->assertEquals($array , replace_recursive($object, $array));
            }
        }
    }

    /**
     */
    public function testMustReplaceArrayElements(): void
    {
        // Run and assert the test.
        $this->assertEquals(
            array ('This', 'is', 'an', 'array', '.', 6),
            replace_recursive(array (), array (1, 2, 3), array (3 => 4, 4 => 5, 5 => 6), array ('This', 'is', 'an', 'array', '.'))
        );
    }

    /**
     */
    public function testMustReplaceObjectProperties(): void
    {
        // Run and assert the test.
        $this->assertEquals(
            (object)array ('property 1' => 'value', 'property 2' => 'value 2', 'prop1' => 'val1', 'prop2' => 'val'),
            replace_recursive(
                (object)array (),
                (object)array ('property 1' => 'value 1', 'property 2' => 'value 2', 'prop1' => 'val1', 'prop2' => 'val2'),
                (object)array ('property 1' => 'value'),
                (object)array ('prop2' => 'val')
            )
        );
    }

    /**
     */
    public function testMustRecursivelyReplaceArrayElements(): void
    {
        // Run and assert the test.
        $this->assertEquals(
            array (array (array (2 => true, 4 => false))),
            replace_recursive(array (array (array (2 => true))), array (array (array (4 => false))))
        );
    }

    /**
     */
    public function testMustRecursivelyReplaceObjectProperties(): void
    {
        // Run and assert the test.
        $this->assertEquals(
            (object)array ('attribute' => (object)array ('property' => (object)array ('attr' => 'val', 'prop' => 'val'))),
            replace_recursive(
                (object)array ('attribute' => (object)array ('property' => (object)array ('attr' => 'val'))),
                (object)array ('attribute' => (object)array ('property' => (object)array ('prop' => 'val')))
            )
        );
    }
}
